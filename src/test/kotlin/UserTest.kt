import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class UserTest {
    var user = User(1, "Alice")

    @Test
    fun `should be able to increase reputation`() {
        // Act
        user.changeReputation(10)
        // Assert
        assertEquals(10, user.reputation)
    }

    @Test
    fun `should be able to decrease reputation`() {
        // Act
        user.changeReputation(10)
        user.changeReputation(-5)
        // Assert
        assertEquals(5, user.reputation)
    }
}